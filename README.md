# Laboratory Class in Particle Physics

Jupyter Kernel with ROOT for the Laboratory Class in Particle Physics offered at the RWTH physics institute 3A/B.
Open profile in RWTH Jupyter: [![](https://jupyter.pages.rwth-aachen.de/documentation/images/badge-launch-rwth-jupyter.svg)](https://jupyter.rwth-aachen.de/hub/spawn?profile=wblc)